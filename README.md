The purpose of these projects is to help me pratice and improve my swift coding skill and project management skill.

1. myFirstApp:	This app is a simple game that requires players to do logical deduction.
		The game asks the user to find a way to make both Button 1 and 2 disappear.

2. UnderWaterCam: 	The purpose of this project is to create a functional camera app for underwater usages, including taking photos in the pool, as mobile devices with water-resistance becoming more and more common.
			The app will allow users to use the touchscreen outside of water as well as to use phsical buttons to control the camera underwater.
