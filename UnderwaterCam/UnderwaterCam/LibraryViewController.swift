//
//  LibraryViewController.swift
//  UnderwaterCam
//
//  Created by Kingsley Wen on 8/15/18.
//  Copyright © 2018 Kingsley Wen. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController {

    var takenPhotos:UIImage?
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Show taken images if there is any
        if let availableImage = takenPhotos {
            imageView.image = availableImage
        }
    }
    
    // Go back to the camera
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
