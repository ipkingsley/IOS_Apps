//
//  ViewController.swift
//  UnderwaterCam
//
//  Created by Kingsley Wen on 8/3/18.
//  Copyright © 2018 Kingsley Wen. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    let captureSession = AVCaptureSession()
    
    var previewLayer:CALayer!
    var captureDevice:AVCaptureDevice!
    
    var takePhoto = false
    var takeVideo = false
    var underwater = false    
    
    @IBOutlet weak var underwaterMode: UIButton!
    
    @IBOutlet weak var Video: UIButton!
    
    @IBOutlet weak var Camera: UIButton!
    
    @IBOutlet weak var Library: UIButton!
    
    @IBOutlet weak var Display: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareCamera()
    }
    
    // Enter Underwater Mode
    @IBAction func underwaterAction(_ sender: UIButton) {
        // Prompt message to user
        underwaterMode.setTitle("Underwater Mode: On", for: .normal)
        
        hideVolumeControl()
        
    }
    
    // Hide volume control panel when using underwater mode
    func hideVolumeControl() {
        let volumeView = MPVolumeView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        view.addSubview(volumeView)
    }
    
    func prepareCamera() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        
        // Finding camera to capture images or videos
        let availableDevices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back).devices
        
        if !(availableDevices.isEmpty) {
            captureDevice = availableDevices.first
            beginSession()
        }
    }
    
    func beginSession() {
        
        // Catch error
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            
            captureSession.addInput(captureDeviceInput)
            
        }catch {
            print(error.localizedDescription)
        }
        
        // Create a preview layer and start the session
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer = previewLayer
        self.view.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.view.layer.frame
        captureSession.startRunning()
        
        // Get data output
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString):NSNumber(value:kCVPixelFormatType_32BGRA)] as [String : Any]
        
        dataOutput.alwaysDiscardsLateVideoFrames = true
        
        if captureSession.canAddOutput(dataOutput) {
            captureSession.addOutput(dataOutput)
        }
        
        captureSession.commitConfiguration()
        
        let queue = DispatchQueue(label: "KingsleyWen.UnderwaterCam.captureQueue")
        dataOutput.setSampleBufferDelegate(self, queue: queue)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func VideoAction(_ sender: UIButton) {
        takeVideo = true
        /*
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.sourceType = .camera
        
        present(picker, animated: true, completion: nil)
        */
    }
    
    @IBAction func CameraAction(_ sender: UIButton) {
        takePhoto = true
        /*
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.sourceType = .camera
        
        present(picker, animated: true, completion: nil)
        */
    }
    
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
        if takePhoto {
            takePhoto = false
            
            if let image = self.getImageFromSampleBuffer(buffer: sampleBuffer) {
                
                let libraryVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LibraryVC") as! LibraryViewController
                
                libraryVC.takenPhotos = image
                
                DispatchQueue.main.sync {
                    self.present(libraryVC, animated: true, completion: { self.stopCaptureSession() })
                }
 
            }
        }
        
        if takeVideo {
            
        }
    }
    
    
    // Stop capture session when viewing photo library
    func stopCaptureSession() {
        self.captureSession.stopRunning()
        
        if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                self.captureSession.removeInput(input)
            }
        }
    }
    
    func getImageFromSampleBuffer (buffer: CMSampleBuffer) -> UIImage?{
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer) {
            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let context = CIContext()
            
            let imageWindow = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer))
            
            if let image = context.createCGImage(ciImage, from: imageWindow) {
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: .right)
            }
        }
        
        return nil
    }
    
    
    @IBAction func LibraryAction(_ sender: UIButton) {
        
        /*
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.sourceType = .photoLibrary
        
        present(picker, animated: true, completion: nil)
        */
    }
    
}

