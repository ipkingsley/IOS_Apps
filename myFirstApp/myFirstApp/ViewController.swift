//
//  ViewController.swift
//  myFirstApp
//
//  Created by Kingsley Qian Wen on 6/15/18.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func btnDisappear(_ sender: UIButton) {
        sender.isHidden = true
    }
    @IBAction func reset(_ sender: UIButton) {
        count = 0
        btn1.isHidden = false
        btn2.isHidden = false
        output.text = "Game reset!"
    }
    
    @IBOutlet weak var output: UITextField!
    var count = 0
    @IBAction func toggleMessage(_ sender: UISwitch) {
        if(count%100 == 99) {
            btn1.isHidden = true
            btn2.isHidden = true
        }
        if(count%2 == 0) {
            btn1.isHidden = false
        }
        else if(count%3 == 1) {
            btn2.isHidden = false
        }
        else if(count%4 == 2) {
            btn2.isHidden = true
        }
        else {
            btn1.isHidden = true
        }
        count = count + 1
        if(sender.isOn) {
            output.text = "My name is Kingsley Qian Wen"
        }
        else{
            output.text = "This is my first ios app"
        }
    }
    @IBAction func b1To2(_ sender: UIButton) {
        if(count%2 == 0) {
            btn2.isHidden = false
        }
        else if(count%5 == 4) {
            btn2.isHidden = true
        }
        else {
            btn1.isHidden = true
        }
    }
    @IBAction func b2To1(_ sender: UIButton) {
        if(count%4 == 2) {
            btn2.isHidden = true
        }
        else{
            btn1.isHidden = true
        }
    }
    @IBAction func showCount(_ sender: UIButton) {
        output.text = "\(count)"
    }
    
    @IBAction func confirm(_ sender: UIButton) {
        if(btn1.isHidden == true && btn2.isHidden == true) {
            output.text = "You did it!"
        }
    }
}

